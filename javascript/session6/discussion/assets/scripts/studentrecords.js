// console.log("hello from B55")

let b55 = [];

// function to add students

function enroll(name, gender, course){
	let student = {};
	student.name = name;
	student.gender = gender;
	student.course = course;
	student.maxAbsences = 5;

	// // option 2
	// let student = {
	// 	name = name,
	// 	gender = gender,
	// 	course = course,
	// 	}

	// // option 3 if parameter and object is same

	// let student = {
	// 	name,
	// 	gender,
	// 	course,
	// 	}
	b55.push(student);
	return student;
}

// enroll("Brandon", "male", "comSci");


function showStudents(){
	b55.forEach(function(student, index){
		console.log("Student ID: " + (index+1) + ", Student Name: " + student.name + ", " + "Gender: " + student.gender + ", " + "Course: " + student.course + ", maxAbsences: " + student.maxAbsences)
	});
}

function markAsAbsent(studentNo){
	let realIndex = studentNo -1 ;
	b55.forEach(function(student, index){
		if (index === realIndex){
			student.maxAbsences--
		}
	})
		
}