// console.log("Dom Manipulation")

// DOM = document object model

// console.log(document.body);

	// getElementById

// let h1 = document.getElementById("main-title");

// console.log(h1);

// let stud2 = document.getElementById("stud-2");

// console.log(stud2);


	// getElementsByTagName

// let header = document.getElementsByTagName("h1")
// console.log(header);

// let studs = document.getElementsByTagName("ul")
// console.log(studs);

// console.log(studs[0].children);


		// getElementsByClassName

// let studlist = document.getElementsByClassName("stud-list");
// // console.log(studlist[0].children[1].innerHTML);
// console.log(studlist[0].textContent);
// or console.log(studlist[0].children[1].textContent);

		// querySelector

// let head1 = document.querySelector("li + li");
// 	console.log(head1);


// let head1 = document.querySelectorAll("li + li");
// 	console.log(head1);


const h1 = document.getElementById("main-title");
h1.style.backgroundColor = "red";
h1.style.color = "white";
h1.style.fontSize = "50px";
h1.style.textAlign = "Center"
h1.textContent = "Hello from B55"